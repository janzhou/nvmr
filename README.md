# NVMR

## Project URLs

1. [Project Home](https://janzhou.org/cgit/nvmr/)
2. [Bug Report](https://janzhou.org/bugzilla/buglist.cgi?product=Research&component=NVMR&resolution=---)
3. [Github](https://github.com/janzhou/nvmr)
4. [Bitbucket](https://bitbucket.org/janzhou/nvmr)

## Requirements

1. Java 8

## How to test

### 1. Compile

    bin/nvmr build

### 2. Configure the disk drive

    cp src/main/resources/config.conf

### 3. Setup Test Environment

    bin/nvmr setup

### 4. Run test

    bin/nvmr test
