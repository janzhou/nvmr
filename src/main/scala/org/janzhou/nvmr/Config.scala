package org.janzhou.nvmr

import java.io.File
import com.typesafe.config.ConfigFactory
import collection.JavaConversions._

object config {
  private val _config = {
    val _default = ConfigFactory.load("config")

    val _file = new File("config.conf")
    if( _file.exists() && _file.isFile() ) {
      val parseFile = ConfigFactory.parseFile(_file)
      ConfigFactory.load(parseFile)
      .withFallback(_default)
    } else {
      _default
    }
  }

  def getBoolean(_c:String):Boolean = {
    _config.getBoolean(_c)
  }

  def getString(_c:String):String = {
    _config.getString(_c)
  }

  def getStringList(_c:String):List[String] = {
    _config.getStringList(_c).toList
  }

  def getInt(_c:String):Int = {
    _config.getInt(_c)
  }

  def getLong(_c:String):Long= {
    _config.getLong(_c)
  }

  def getDouble(_c:String):Double = {
    _config.getDouble(_c)
  }
}
