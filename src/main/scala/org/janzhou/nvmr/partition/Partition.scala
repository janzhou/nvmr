package org.janzhou.nvmr.partition

import org.janzhou.nvmr.console
import org.janzhou.nvmr.storage._
import org.janzhou.nvmr.ldd._
import scala.collection.GenTraversableOnce
import scala.reflect.ClassTag
import java.util.concurrent.locks._

object Partition {
  private var _id = 0L

  def get_id:Long = {
    var id = 0L
    this.synchronized {
      id = _id
      _id += 1
    }
    id
  }
}

abstract class Partition[T:ClassTag] extends Serializable {
  private var loc:(Long, Long) = null
  private var drive:Int = -1

  val id = Partition.get_id
  private val lock = new ReentrantLock()

  private var _cache:Array[T] = null
  private var _compute = false
  private var _persist = false
  private var _gc      = false

  /** compute once */
  def one:Array[T]

  final def compute = {
    this.synchronized {
      if ( !_compute ){
        console.debug("Compute partition " + id)
        _cache = one
        _compute = true
        if ( _persist ) persist()
      }
    }
  }

  final def persist(dev:Int = 0) = {
    this.synchronized {
      _persist = true
      if ( drive == -1 ) drive = dev
      assert( drive >= 0 )
      assert( drive < Storage.logs.length )
      if ( _compute ) {
        if ( loc == null ) {
          console.debug("Persist partition " + id)
          loc = Storage.logs(drive).store(_cache)
          if ( _gc ) {
            gc
          }
        }
      }
  }
  }

  def trim = {
    if( loc != null ) {
      Storage.logs(drive).trim(loc)
      loc = null
    }
  }

  private def load:Array[T] = {
    if ( loc != null ) {
      Storage.logs(drive).load[Array[T]](loc)
    } else null
  }

  /** load _cache */
  final def cache:Array[T] = {
    this.synchronized {
      _gc = false
      if ( !_compute ) compute
      if ( _cache == null ){
          _cache = load
      }
      _cache
    }
  }

  /** clean _cache */
  final def gc = {
    this.synchronized {
      _gc = true
      if( _compute && _persist ) {
        console.debug("GC partition " + id)
        _cache = null
      }
    }
  }

  final def content: Array[T] = cache

  def map[V:ClassTag](v:(T => V)):MapFunctionPartition[V, T] = {
    new MapFunctionPartition(this, v)
  }

  def flatMap[V:ClassTag](v:(T => GenTraversableOnce[V])):FlatMapPartition[V, T] = {
    new FlatMapPartition(this, v)
  }

  def groupBy[V:ClassTag](v:(T => V)):GroupByPartition[V, T] = {
    new GroupByPartition(this, v)
  }

  def filter(v:(T => Boolean)):FilterPartition[T] = {
    new FilterPartition(this, v)
  }

  def sortWith(v:((T, T) => Boolean)):SortPartition[T] = {
    new SortPartition(this, v)
  }

  def reduce(f: (T, T) => T):T = {
    content.reduce(f)
  }

  def toMapPartition[U:ClassTag, V:ClassTag](f: T => (U, V)):MapPartition[U, V] = {
    new toMapPartition(this, f)
  }

  def setReduceParrent(_p:LDD[T]):Unit = Unit

  override def toString():String = content.map(i => i.toString).reduce[String]{(a, b) => a + ", " + b}
}
