package org.janzhou.nvmr.partition

import scala.reflect.ClassTag
import scala.collection.GenTraversableOnce

import org.janzhou.nvmr.pmemory._

class FlatMapPartition[T:ClassTag, U:ClassTag](
  @transient private var p:Partition[U],
  @transient private var f:(U => GenTraversableOnce[T])) extends Partition[T] {

  override def one = {
    val cache = p.content.flatMap(f)
    p = null
    f = null
    cache
  }
}
