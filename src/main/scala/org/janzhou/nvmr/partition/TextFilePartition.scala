package org.janzhou.nvmr.partition

import scala.reflect.ClassTag
import scala.io.Source
import scala.io.Codec
import org.janzhou.nvmr._
import org.janzhou.nvmr.pmemory._
import org.janzhou.nvmr.storage.Driver
import java.io.ByteArrayInputStream
import java.nio.charset.CodingErrorAction

object TextFilePartition {
  val sync = config.getBoolean("TextFileLDD.sync")
}

class TextFilePartition(loc:(String, Long, Long)) extends Partition[String] {
  private val file:String = loc._1
  private val offset:Long = loc._2
  private val size:Long = loc._3

  override def one = {
    implicit val codec = Codec("UTF-8")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    console.log("Load TextFilePartition " + id)

    if( offset == -1L ) {
      Source.fromFile(file).getLines.toArray
    } else {
      val driver = new Driver(file)
      val buf = {
        if( TextFilePartition.sync ) {
          TextFilePartition.synchronized {
            driver.read(offset, size)
          }
        } else {
          driver.read(offset, size)
        }
      }
      val input = new ByteArrayInputStream(buf.array())
      Source.fromInputStream(input).getLines.toArray
    }
  }
}
