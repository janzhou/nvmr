package org.janzhou.nvmr

import org.janzhou.nvmr._
import org.janzhou.nvmr.partition._
import org.janzhou.nvmr.executor._
import org.janzhou.nvmr.ldd._

import java.time.{Instant, Duration}

object WordCountTest {
  def textFile(dir:String):TextFileLDD = {
    new TextFileLDD(dir)
  }
  def main (args: Array[String]) {
    //console.print_level = console.print_debug
    //val file = "/home/jan/data/enwikisource-20150901-pages-meta-history.xml"
    val file = args(0)
    val sample =
      textFile(file)
      //.samplePartitions(1000)
      .flatMap(line => line.split(" "))
      // .flatMap(line => line.replace(']', ' ')
                            // .replace('[', ' ')
                            // .replace('{', ' ')
                            // .replace('}', ' ')
                            // .replace('(', ' ')
                            // .replace(')', ' ')
                            // .replace(',', ' ')
                            // .replace('.', ' ')
                            // .replace(':', ' ')
                            // .replace('_', ' ')
                            // .replace('/', ' ')
                            // .replace('=', ' ')
                            // .replace('>', ' ')
                            // .replace('<', ' ')
                            // .replace('\n', ' ')
                            // .replace('*', ' ')
                            // .replace('"', ' ')
                            // .replace('!', ' ')
                            // .replace('+', ' ')
                            // .replace('-', ' ')
                            // .replace('&', ' ')
                            // .replace('@', ' ')
                            // .replace(';', ' ')
                            // .replace('*', ' ')
                            // .replace('|', ' ')
           // .split(" "))
           //.toMapLDD(m => (m, 1)).reduceByKey( _ + _ )
    // val results = sample.sortWithPartitions( _._1 < _._1 )

    Executor.gc
    sample.persist
    sample.gc
    val start = Instant.now()
    sample.compute
    val end = Instant.now()
    Executor.gc
    Executor.finalize
    Executor.gc

    println(Duration.between(start, end))
  }
}
